import express from 'express';
import exphbs from 'express-handlebars';
import path from 'path';
import { LIFECYCLE_PAGES } from './constants';
import helpers from './lib/helpers';

const app = express();

app.engine(
  'handlebars',
  exphbs({
    defaultLayout: 'main',
    layoutsDir: path.resolve(__dirname, './views/layouts'),
    partialsDir: path.resolve(__dirname, './views/partials'),
    helpers,
  }),
);
app.set('view engine', 'handlebars');
app.set('views', path.resolve(__dirname, './views'));
app.use(express.static(path.resolve(__dirname, './public')));

app.get('/', (req, res) =>
  res.render('home', {
    lifecycleLinks: LIFECYCLE_PAGES.map(page => ({ title: page.title, key: page.key })),
    scriptFile: path.resolve(__dirname, './public/js/offline.js'),
  }),
);

app.get('/lifecycle/:key', (req, res) =>
  res.render('lifecycle', {
    activePage: LIFECYCLE_PAGES.filter(link => link.key === req.params.key)[0],
    lifecycleLinks: LIFECYCLE_PAGES.map(page => ({ title: page.title, key: page.key })),
    scriptFile: path.resolve(__dirname, `./public/js/lifecycle/${req.params.key}.js`),
  }),
);

app.get('/cacheapi', (req, res) =>
  res.render('cacheapi', {
    scriptFile: path.resolve(__dirname, './public/js/cacheapi.js'),
  }),
);

app.get('/offline', (req, res) => res.render('offline'));

app.get('/prpl', (req, res) =>
  res.render('prpl/shell', {
    layout: false,
    shellStyle: path.resolve(__dirname, './public/css/prpl/shell.css'),
    scriptFile: path.resolve(__dirname, './public/js/prpl/shell.js'),
  }),
);

app.get('/prpl/page', (req, res) =>
  res.render('prpl/page', {
    layout: false,
    // shellStyle: path.resolve(__dirname, './public/css/prpl/shell.css'),
    // scriptFile: path.resolve(__dirname, './public/js/prpl/shell.js'),
  }),
);

app.get('/prpl/page2', (req, res) =>
  res.render('prpl/page2', {
    layout: false,
    // shellStyle: path.resolve(__dirname, './public/css/prpl/shell.css'),
    scriptFile: path.resolve(__dirname, './public/js/prpl/shell.js'),
  }),
);

app.get('/push', (req, res) =>
  res.render('push'),
);

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
