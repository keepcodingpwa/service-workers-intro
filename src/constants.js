export const METAS = {
  title: 'Service Workers Intro',
};

export const LIFECYCLE_PAGES = [
  {
    key: 'parsed',
    title: 'Analizado',
  },
  {
    key: 'installing',
    title: 'Instalando',
  },
  {
    key: 'installed',
    title: 'Instalado',
  },
  {
    key: 'activating',
    title: 'Activando',
  },
  {
    key: 'activated',
    title: 'Activado',
  },
  {
    key: 'redundant',
    title: 'Redundante',
  },
];

export const MAIN_LINKS = [
  {
    url: `/lifecycle/${LIFECYCLE_PAGES[0].key}`,
    title: 'Ciclo de vida',
    intro: 'Se analiza y valida el script y devuelve un acceso al objeto<b>registration</b>',
  },
  {
    url: '/cacheapi',
    title: 'Cache API',
    intro: 'Cache API intro',
  },
  {
    url: '/prpl',
    title: 'PRPL',
    intro: 'PRPL intro',
  },
  {
    url: '/push',
    title: 'Push',
    intro: 'Push intro',
  },
];
