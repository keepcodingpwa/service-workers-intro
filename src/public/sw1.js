const cacheVersion = 1;
const currentCache = {
  offline: `offline-cache${cacheVersion}`,
};
const offlineUrl = '/offline';

this.addEventListener('install', event => {
  event.waitUntil(
    caches
      .open(currentCache.offline)
      .then(cache => cache.addAll(['/', '/css/tachyons.css', offlineUrl])),
  );
});

this.addEventListener('fetch', event => {
  if (
    event.request.mode === 'navigate' ||
    (event.request.method === 'GET' && event.request.headers.get('accept').includes('text/html'))
  ) {
    event.respondWith(
      fetch(event.request.url).catch(error =>
        // Return the offline page
        caches.match(offlineUrl),
      ),
    );
  } else {
    // Respond with everything else if we can
    event.respondWith(
      caches.match(event.request).then(response => response || fetch(event.request)),
    );
  }
});
