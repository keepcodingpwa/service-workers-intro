// Initialize required variables
const shellCacheName = 'pwa-keepcoding-v1';
const filesToCache = [
  '/',
  '/prpl',
  '/prpl/page',
  '/prpl/page2',
  '/css/tachyons.css',
  '/js/prpl/page.js',
];

// Listen to installation event
self.addEventListener('install', e => {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(shellCacheName).then(cache => {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    }),
  );
});

self.addEventListener('activate', e => {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    // Get all cache containers
    caches.keys().then(keyList =>
      Promise.all(
        keyList.map(key => {
          // Check and remove invalid cache containers
          if (key !== shellCacheName) {
            console.log('[ServiceWorker] Removing old cache', key);
            return caches.delete(key);
          }
        }),
      ),
    ),
  );

  // Enforce immediate scope control
  return self.clients.claim();
});

// Listen to fetching event
self.addEventListener('fetch', (e) => {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then((response) => response || fetch(e.request)),
  );
});
