const $message = document.querySelector('#message');

if ('serviceWorker' in navigator) {
  document.querySelector('#clear-cache').addEventListener('click', () => {
    navigator.serviceWorker
      .getRegistrations()
      .then(registrations => registrations.map(registration => registration.unregister()))
      .then(() => {
        $message.innerHTML += '<p>Se han limpiado todos los Service Workers</p>';
      });
  });
  document.querySelector('#register-sw-1').addEventListener('click', () => {
    navigator.serviceWorker
      .register('/sw1.js')
      .then(registration => {
        $message.innerHTML += '<p>El SW se ha registrado correctamente</p>';
      })
      .catch(err => {
        $message.innerHTML += '<p>No se ha podido registrar el SW</p>';
      });
  });
  document.querySelector('#register-sw-2').addEventListener('click', () => {
    navigator.serviceWorker
      .register('/sw2.js')
      .then(registration => {
        $message.innerHTML += '<p>El SW 2 se ha registrado correctamente</p>';
      })
      .catch(err => {
        $message.innerHTML += '<p>No se ha podido registrar el SW 2</p>';
      });
  });
}
