// Register the service worker
if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/sw1.js')
    .then((registration) => {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    })
    .catch((err) => {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
}
