const API_KEY =
  'AAAAJmdedFs:APA91bGiuXf17lsw1xLsBF2Wn5yMrGGkUA4ClzQ-lA1Bf2ZQnmX6jfW03e-V_Jj8yk5pwaP7OU4O0wt1Q37LflU_6qs7vec821A9LTwSmsbziZXGKedOJkexW7IYr2GH0mPi-sWmOzNl';
const GCM_ENDPOINT = 'https://android.googleapis.com/gcm/send';

const curlCommandDiv = document.querySelector('.js-curl-command');
let isPushEnabled = false;

// This method handles the removal of subscriptionId
// in Chrome 44 by concatenating the subscription Id
// to the subscription endpoint
function endpointWorkaround(pushSubscription) {
  // Make sure we only mess with GCM
  if (pushSubscription.endpoint.indexOf('https://android.googleapis.com/gcm/send') !== 0) {
    return pushSubscription.endpoint;
  }

  let mergedEndpoint = pushSubscription.endpoint;
  // Chrome 42 + 43 will not have the subscriptionId attached
  // to the endpoint.
  if (
    pushSubscription.subscriptionId &&
    pushSubscription.endpoint.indexOf(pushSubscription.subscriptionId) === -1
  ) {
    // Handle version 42 where you have separate subId and Endpoint
    mergedEndpoint = `${pushSubscription.endpoint}/${pushSubscription.subscriptionId}`;
  }
  return mergedEndpoint;
}

function sendSubscriptionToServer(subscription) {
  // TODO: Send the subscription.endpoint
  // to your server and save it to send a
  // push message at a later date
  //
  // For compatibly of Chrome 43, get the endpoint via
  // endpointWorkaround(subscription)
  console.log('TODO: Implement sendSubscriptionToServer()');

  const mergedEndpoint = endpointWorkaround(subscription);

  // This is just for demo purposes / an easy to test by
  // generating the appropriate cURL command
  showCurlCommand(mergedEndpoint);
}

// NOTE: This code is only suitable for GCM endpoints,
// When another browser has a working version, alter
// this to send a PUSH request directly to the endpoint
function showCurlCommand(mergedEndpoint) {
  // The curl command to trigger a push message straight from GCM
  if (mergedEndpoint.indexOf(GCM_ENDPOINT) !== 0) {
    window.Demo.debug.log("This browser isn't currently " + 'supported for this demo');
    return;
  }

  const endpointSections = mergedEndpoint.split('/');
  const subscriptionId = endpointSections[endpointSections.length - 1];

  const curlCommand =
    `curl --header "Authorization: key=${
      API_KEY
    }" --header Content-Type:"application/json" ${
      GCM_ENDPOINT
    } -d "{\\"registration_ids\\":[\\"${
      subscriptionId
    }\\"]}"`;

  curlCommandDiv.textContent = curlCommand;
}

function unsubscribe() {
  const pushButton = document.querySelector('.js-push-button');
  pushButton.disabled = true;
  curlCommandDiv.textContent = '';

  navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
    // To unsubscribe from push messaging, you need get the
    // subcription object, which you can call unsubscribe() on.
    serviceWorkerRegistration.pushManager
      .getSubscription()
      .then((pushSubscription) => {
        // Check we have a subscription to unsubscribe
        if (!pushSubscription) {
          // No subscription object, so set the state
          // to allow the user to subscribe to push
          isPushEnabled = false;
          pushButton.disabled = false;
          pushButton.textContent = 'Habilitar';
          return;
        }

        // TODO: Make a request to your server to remove
        // the users data from your data store so you
        // don't attempt to send them push messages anymore

        // We have a subcription, so call unsubscribe on it
        pushSubscription
          .unsubscribe()
          .then(() => {
            pushButton.disabled = false;
            pushButton.textContent = 'Habilitar';
            isPushEnabled = false;
          })
          .catch((e) => {
            // We failed to unsubscribe, this can lead to
            // an unusual state, so may be best to remove
            // the subscription id from your data store and
            // inform the user that you disabled push

            window.Demo.debug.log('Unsubscription error: ', e);
            pushButton.disabled = false;
          });
      })
      .catch((e) => {
        window.Demo.debug.log('Error thrown while unsubscribing from ' + 'push messaging.', e);
      });
  });
}

function subscribe() {
  // Disable the button so it can't be changed while
  // we process the permission request
  const pushButton = document.querySelector('.js-push-button');
  pushButton.disabled = true;

  navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
    serviceWorkerRegistration.pushManager
      .subscribe({ userVisibleOnly: true })
      .then((subscription) => {
        // The subscription was successful
        isPushEnabled = true;
        pushButton.textContent = 'Deshabilitar';
        pushButton.disabled = false;

        // TODO: Send the subscription subscription.endpoint
        // to your server and save it to send a push message
        // at a later date
        return sendSubscriptionToServer(subscription);
      })
      .catch((e) => {
        if (Notification.permission === 'denied') {
          // The user denied the notification permission which
          // means we failed to subscribe and the user will need
          // to manually change the notification permission to
          // subscribe to push messages
          window.Demo.debug.log('Permission for Notifications was denied');
          pushButton.disabled = true;
        } else {
          // A problem occurred with the subscription, this can
          // often be down to an issue or lack of the gcm_sender_id
          // and / or gcm_user_visible_only
          window.Demo.debug.log('Unable to subscribe to push.', e);
          pushButton.disabled = false;
          pushButton.textContent = 'Habilitar';
        }
      });
  });
}

// Once the service worker is registered set the initial state
function initialiseState() {
  // Are Notifications supported in the service worker?
  if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
    window.Demo.debug.log("Notifications aren't supported.");
    return;
  }

  // Check the current Notification permission.
  // If its denied, it's a permanent block until the
  // user changes the permission
  if (Notification.permission === 'denied') {
    window.Demo.debug.log('The user has blocked notifications.');
    return;
  }

  // Check if push messaging is supported
  if (!('PushManager' in window)) {
    window.Demo.debug.log("Push messaging isn't supported.");
    return;
  }

  // We need the service worker registration to check for a subscription
  navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
    // Do we already have a push message subscription?
    serviceWorkerRegistration.pushManager
      .getSubscription()
      .then((subscription) => {
        // Enable any UI which subscribes / unsubscribes from
        // push messages.
        const pushButton = document.querySelector('.js-push-button');
        pushButton.disabled = false;

        if (!subscription) {
          // We aren’t subscribed to push, so set UI
          // to allow the user to enable push
          return;
        }

        // Keep your server in sync with the latest subscription
        sendSubscriptionToServer(subscription);

        // Set your UI to show they have subscribed for
        // push messages
        pushButton.textContent = 'Deshabilitar';
        isPushEnabled = true;
      })
      .catch((err) => {
        window.Demo.debug.log('Error during getSubscription()', err);
      });
  });
}

window.addEventListener('load', () => {
  const pushButton = document.querySelector('.js-push-button');
  pushButton.addEventListener('click', () => {
    if (isPushEnabled) {
      unsubscribe();
    } else {
      subscribe();
    }
  });

  // Check that service workers are supported, if so, progressively
  // enhance and add push messaging support, otherwise continue without it.
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw4.js').then(initialiseState);
  } else {
    window.Demo.debug.log("Service workers aren't supported in this browser.");
  }
});
