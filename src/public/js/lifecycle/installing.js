const $message = document.querySelector('#message');
if ('serviceWorker' in navigator) {
  $message.innerHTML = '<p>¡Tu navegador soporta Service Workers!</p>';

  // primero deregistramos y luego registramos el SW
  navigator.serviceWorker
    .getRegistrations()
    .then(registrations => registrations.map(registration => registration.unregister()))
    .then(() => {
      navigator.serviceWorker
        .register('/sw2.js')
        .then(registration => {
          console.log(registration);
          if (registration.installing) {
            $message.innerHTML += '<p>El SW se está instalando</p>';
            console.info('Service Worker instalando', registration.installing);
          }
        })
        .catch(err => {
          console.error('El Service Worker no se puedo registrar', err);
        });
    });
}
