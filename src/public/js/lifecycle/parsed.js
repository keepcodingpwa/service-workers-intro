const $message = document.querySelector('#message');
if ('serviceWorker' in navigator) {
  $message.innerHTML = '<p>¡Tu navegador soporta Service Workers!</p>';
  navigator.serviceWorker
    .register('/sw2.js')
    .then(registration => {
      console.log(registration);
      $message.innerHTML += '<p>El SW se analizó de forma satistactoria</p>';
      console.info('Service Worker analizado', registration);
    })
    .catch(err => {
      console.error('El Service Worker no se puedo registrar', err);
    });
}
