const $message = document.querySelector('#message');
if ('serviceWorker' in navigator) {
  $message.innerHTML = '<p>¡Tu navegador soporta Service Workers!</p>';

  // primero deregistramos y luego registramos el SW
  navigator.serviceWorker
    .getRegistrations()
    .then(registrations => registrations.map(registration => registration.unregister()))
    .then(() => {
      navigator.serviceWorker
        .register('/sw2.js')
        .then(registration => {
          registration.onupdatefound = () => {
            const installingWorker = registration.installing;
            installingWorker.onstatechange = () => {
              if (installingWorker.state === 'activating') {
                $message.innerHTML += '<p>El SW está activando</p>';
                console.info('Service Worker activando', installingWorker);
              }
            };
          };
        })
        .catch(err => {
          console.error('El Service Worker no se puedo registrar', err);
        });
    });
}
