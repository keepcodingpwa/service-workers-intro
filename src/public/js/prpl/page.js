document.querySelector('#load-page').addEventListener('click', () => {
  injectScript('https://cdnjs.cloudflare.com/ajax/libs/prism/1.11.0/prism.min.js').then(
    () => {
      injectScript(
        'https://cdnjs.cloudflare.com/ajax/libs/prism/1.11.0/components/prism-javascript.min.js',
      );
    },
  );
  axios.get('/prpl/page2').then(response2 => {
    if (response2.status === 200) {
      document.querySelector('#page-content').innerHTML = response2.data;
    }
  });
});