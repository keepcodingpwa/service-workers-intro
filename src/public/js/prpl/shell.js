function injectScript(src) {
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.async = true;
    script.src = src;
    script.addEventListener('load', resolve);
    script.addEventListener('error', () => reject('Error loading script.'));
    script.addEventListener('abort', () => reject('Script loading aborted.'));
    document.head.appendChild(script);
  });
}

injectScript('https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js')
  .then(() => {
    axios.get('/prpl/page').then(response => {
      if (response.status === 200) {
        document.querySelector('#content').innerHTML = response.data;
        injectScript('/js/prpl/page.js');
      }
    });
  })
  .catch(error => {
    console.log(error);
  });

navigator.serviceWorker
  .register('/sw3.js')
  .then(registration => {
    console.log('SW registrado, ahora se servirá el contenido desde caché en caso de ser posible');
  })
  .catch(err => {
    console.log('Ha habido un problema registrando el SW');
  });
