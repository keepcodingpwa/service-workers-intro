import createHandlebarsHelpers from 'handlebars-helpers';
import { MAIN_LINKS, METAS } from '../constants';

const handlebarsHelpers = createHandlebarsHelpers();

export default {
  ...handlebarsHelpers,
  getTitle: () => METAS.title,
  getMainLinks: () =>
    MAIN_LINKS.map(
      link => `
    <a href="${link.url}" class="f6 fw6 hover-blue link black-70 mr2 mr3-m mr4-l dib">
      ${link.title}
    </a>
  `,
    ).join(''),
};
