# Introducción a los Service Workers #

Ejemplos básicos para entender el funcionamiento de los SW

### Ciclo de Vida ###

* Analizado (parsed)
* Instalando (installing)
* Instalado (installed)
* Activando (activating)
* Activo (active)